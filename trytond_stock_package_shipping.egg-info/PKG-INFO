Metadata-Version: 2.1
Name: trytond_stock_package_shipping
Version: 7.0.1
Summary: The package shipping module of the Tryton application platform.
Home-page: http://www.tryton.org/
Download-URL: http://downloads.tryton.org/7.0/
Author: Tryton
Author-email: foundation@tryton.org
License: GPL-3
Project-URL: Bug Tracker, https://bugs.tryton.org/
Project-URL: Documentation, https://docs.tryton.org/
Project-URL: Forum, https://www.tryton.org/forum
Project-URL: Source Code, https://code.tryton.org/tryton
Keywords: tryton stock package shipping
Classifier: Development Status :: 5 - Production/Stable
Classifier: Environment :: Plugins
Classifier: Framework :: Tryton
Classifier: Intended Audience :: Developers
Classifier: Intended Audience :: Financial and Insurance Industry
Classifier: Intended Audience :: Legal Industry
Classifier: License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)
Classifier: Natural Language :: Bulgarian
Classifier: Natural Language :: Catalan
Classifier: Natural Language :: Czech
Classifier: Natural Language :: Dutch
Classifier: Natural Language :: English
Classifier: Natural Language :: Finnish
Classifier: Natural Language :: French
Classifier: Natural Language :: German
Classifier: Natural Language :: Hungarian
Classifier: Natural Language :: Indonesian
Classifier: Natural Language :: Italian
Classifier: Natural Language :: Persian
Classifier: Natural Language :: Polish
Classifier: Natural Language :: Portuguese (Brazilian)
Classifier: Natural Language :: Romanian
Classifier: Natural Language :: Russian
Classifier: Natural Language :: Slovenian
Classifier: Natural Language :: Spanish
Classifier: Natural Language :: Turkish
Classifier: Natural Language :: Ukrainian
Classifier: Operating System :: OS Independent
Classifier: Programming Language :: Python :: 3
Classifier: Programming Language :: Python :: 3.8
Classifier: Programming Language :: Python :: 3.9
Classifier: Programming Language :: Python :: 3.10
Classifier: Programming Language :: Python :: 3.11
Classifier: Programming Language :: Python :: 3.12
Classifier: Programming Language :: Python :: Implementation :: CPython
Classifier: Topic :: Office/Business
Requires-Python: >=3.8
License-File: LICENSE
Requires-Dist: python-sql>=0.4
Requires-Dist: trytond_carrier<7.1,>=7.0
Requires-Dist: trytond_product<7.1,>=7.0
Requires-Dist: trytond_stock<7.1,>=7.0
Requires-Dist: trytond_stock_package<7.1,>=7.0
Requires-Dist: trytond_stock_shipment_measurements<7.1,>=7.0
Requires-Dist: trytond_stock_shipment_cost<7.1,>=7.0
Requires-Dist: trytond_product_measurements<7.1,>=7.0
Requires-Dist: trytond<7.1,>=7.0
Provides-Extra: test
Requires-Dist: proteus<7.1,>=7.0; extra == "test"

Stock Package Shipping Module
#############################

This module is the base module required to interact with shipping service
providers.

Carrier
*******

The Carrier model adds the following field:

- *Shipping Service*: The shipping service of the carrier.

This field is programmatically filled by the modules providing support for
shipping companies.

Package Type
************

The Package Type model has been added the following fields:

- *Length*: The length of the packages of this type
- *Length Unit*: The unit of measure of this length
- *Length Digits*: The precision of length
- *Height*: The height of the packages of this type
- *Height Unit*: The unit of measure of this height
- *Height Digits*: The precision of height
- *Width*: The width of the packages of this type
- *Width Unit*: The unit of measure of this width
- *Width Digits*: The precision of width

Package
*******

The Package model has been added the following fields:

- *Shipping Reference*: The shipping reference provided by the shipping service
- *Shipping Label*: The shipping label provided by the shipping service
- *Weight*: A function field computing the weight of the package with its
  content

Shipment Out
************

The Shipment Out model will check once in the Packed state if the shipment is a
valid shipment for the shipping service. He does that by calling a method that
is by convention named ``validate_packing_<shipping service>``.

Once a shipment is packed, the user can create the shipping for each packages
with the shipping service by clicking on the *Create Shipping* button. This
button triggers a wizard that is overridden in shipping service specific
modules. The starting state of the wizard is a ``StateTransition``. Its linked
method is overridden in shipping service modules in order to communicate with
the service.

